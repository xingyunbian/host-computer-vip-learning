﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Test30();



            Console.ReadLine();
        }

        #region   18.字符串的常用处理方法
        static void Test30()
        {
            string email = "hua252@qq.com";
            char ch1 = '.';
            char ch2 = '@';
            int count = email.Count(f => (f == ch1));
            Console.WriteLine("找到的.的个数"+count);
            count = email.Count(f => (f == ch2));
            Console.WriteLine("找到的@的个数"+count);
            int index1=email.IndexOf("@");
            int index2 = email.IndexOf("qq.com");
            int index3 = email.IndexOf("qq.net");
            Console.WriteLine($"{index1}\t{index2}\t{index3}\t"  );
            int length=email.Length;//获得字符串长度
            Console.WriteLine($"当前字符串的长度:{length}");
            string info = "现在大家学习的是常老师主讲的.NET/C#开发课程";
            Console.WriteLine(  info.IndexOf("常老师"));
            Console.WriteLine(  info.Length);
        }
        #endregion
    }
}
